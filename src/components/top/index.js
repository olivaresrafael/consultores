import { h } from 'preact';
import { useEffect } from 'preact/hooks';
import { Link } from 'preact-router/match';
import Animation from '../animation';
import style from './style.css';


const Top = (props) => {

    return (
        <header class={"masthead " + (props.notHome? style.notHome : "")}>
            <div class="container px-4 px-lg-5 h-100">
                <div class="row gx-4 gx-lg-5 h-100 align-items-center justify-content-center text-center">
                    <div class="col-lg-10 align-self-end">
                        <Animation>
                            <h1 class={style.home_title + " text-white font-weight-bold"}>{props.title}</h1>
                        </Animation>
                        <hr class="divider" />
                    </div>
                    <div class="col-lg-8 align-self-baseline">
                        <Animation>
                            <p class="text-white-75 mb-5">{props.description}</p>
                        </Animation>          
                        {props.button && <Link class="btn btn-primary btn-xl" href="/servicios">Explora nuestros servicos</Link>}
                    </div>
                </div>
            </div>
        </header>
    )
};

export default Top;


