import TrackVisibility from 'react-on-screen';
import { useEffect } from 'preact/hooks';
import style from './style.css';

function Animation(props) {
        
        return (
            <TrackVisibility partialVisibility once>
            {({ isVisible }) => isVisible? <div class={style.tag + " " + style.visible}>{props.children}</div> :
                <div class={style.tag}>{props.children}</div>}
            </TrackVisibility>
        )
}

export default Animation;