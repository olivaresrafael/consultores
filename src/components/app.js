import { h,  } from 'preact';
import { useState } from 'preact/hooks';
import { Router } from 'preact-router';
import ScrollToTop from '../components/scrolltotop'
import Header from './header';
import Top from './top';

// Code-splitting is automated for `routes` directory
import Home from '../routes/home';
import Nosotros from '../routes/nosotros';
import Servicios from '../routes/servicios';
import Trabajos from '../routes/trabajos';
import Contacto from '../routes/contacto';
import model from './model';

const App = () => {
	//const [loading, setLoading] = useState(false);
	const [pathname, setPathname] = useState("/");
	const [top, setTop] = useState([model.title, model.description, false, true]);

	const handleRoute = async e => {
		setPathname(e.url);
		let arrayDeCadenas = e.url.split('#');
		switch (arrayDeCadenas[0]) {
			case '/':
				setTop([model.title, model.description, true, false]);
				break;
			case '/nosotros':
				setTop([model.about.title, model.about.description, false, true]);
				break;
			case '/servicios':
				setTop([model.services.title, model.services.description, false, true]);
				break;
			case '/trabajos':
				setTop([model.work.title, model.work.description, false, true]);
				break;
			case '/contacto':
				setTop([model.contact.title, model.contact.description, false, true]);
				break;
		}
	};

	return (

	<div id="app">
		<Header />
		<ScrollToTop pathname={pathname}>
		<Top 
			title={top[0]} 
			description={top[1]} 
			button={top[2]} 
			notHome={top[3]}
		/>
		<Router onChange={handleRoute}>
				<Home path="/" />
				<Nosotros path="/nosotros" />
				<Servicios path="/servicios" />
				<Trabajos path="/trabajos" />
				<Contacto path="/contacto" />
		</Router>
		</ScrollToTop>
	</div>
	)

	// useEffect(() => {
	// 	setTimeout(() => {
	// 	  setLoading(false);
	// 	}, 2000);
	//   }, []);
	//   return <div>{loading ? <div class="loader"></div> 
	// 	: 
	// 	<div id="app">
	// 		<Header />
	// 		<ScrollToTop pathname={pathname}>
	// 		<Router onChange={handleRoute}>
	// 				<Home path="/" />
	// 				<Nosotros path="/nosotros" />
	// 				<Servicios path="/servicios" />
	// 				<Trabajos path="/trabajos" />
	// 				<Contacto path="/contacto" />
	// 		</Router>
	// 		</ScrollToTop>
	// 	</div>
	//   }</div>;
}

export default App;


