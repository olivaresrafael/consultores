const IndexData =  {
    company : 'Consultores SC',
    title : 'Explora nuestros servicios y expande tu soporte de atención al cliente',
    subtitle : 'Quisque lobortis convallis mauris sit amet facilisis.',
    description : 'Consultores SC cuenta con más de 40 años de amplia experiencia nacional e internacional en la integración de gestión de Collections Agency, Contact Center y BPO.',
    services : {
        title : 'Oferta de Servicios',
        subtitle : 'Quisque lobortis convallis mauris sit amet facilisis.',
        description : 'Experiencia y KNOW HOW para la implementación de una amplia variedad de productos y servicios para los sectores financieros y empresarial tanto a nivel nacional como internacional.',
        list : [
            {
                title : 'Collection Agency',
                description : 'Personal especializado en cobranza y recaudación de servicios. Quisque lobortis convallis mauris sit amet facilisis. In quis dolor iaculis nulla viverra fermentum.',
                icon : 'bi-cash-coin',
                tag : '#agency',
                sub : [
                    {
                        title : 'Cobranza especilizada bancaria',
                        description : 'Servicio outsourcing de estrategia y gestión de cobranza para carteras vigentes y demoradas, orientado a empresas del sector financiero.',
                        icon : 'bi-building',
                    },
                    {
                        title : 'Recaudación de servicios',
                        description : 'Servicio outsourcing de estrategia y gestión de cobranza orientado a empresas de servicios públicos o masivos.',
                        icon : 'bi-cash-stack',
                    },
                ]
            },
            {
                title : 'Contact Center',
                description : 'Tenemos los servicios de agentes en vivo cubiertos en todas las áreas: atención al cliente, ventas, soporte técnico, generación de clientes potenciales y cobros.',
                icon : 'bi-telephone',
                tag : '#ccenter',
                sub : [
                    {
                        title : 'Gestión outbound',
                        description : 'Servicio outsourcing de estrategia y gestión Outbound de nuestros aliados comerciales:',
                        icon : 'bi-telephone-outbound',
                        list : [
                            'Telemarketing',
                            'Promoción',
                            'Campañas de captación',
                            'Encuestas'
                        ]
                    },
                    {
                        title : 'Gestión inbound',
                        description : 'Servicio outsourcing de estrategia y gestión Inbound de nuestros aliados comerciales:',
                        icon : 'bi-telephone-inbound',
                        list : [
                            'Servicio al cliente',
                            'Regularización medios de pago'
                        ]
                    }
                ]
            },
            {
                title : 'B.P.O.',
                description : 'Business process outsourcing es un servicio outsourcing end-to end de procesos',
                icon : 'bi-recycle',
                tag : '#bpo',
                list : [
                    ['Análisis de Riesgo y Proceso de Crédito', 'bi-journal-medical'],
                    ['Renovación de pólizas de seguros', 'bi-arrow-repeat'],
                    ['Atención a contribuyentes', 'bi-chat-dots'],
                    ['Recaudación de primas de seguros', 'bi-cash'],
                    ['Venta de pólizas de seguros', 'bi-briefcase']
                ]
            }
        ],
    },
    about : {
        title : 'Conoce todo sobre nosotros',
        subtitle : 'Somos tu mejor opción en VENEZUELA',
        icon : 'bi-bricks',
        description : 'Somos una empresa operadora de productos y servicios financieros, de atención integral, pionera y líder del mercado, fundada en 1979. Conceptualizamos, desarrollamos procesos y operamos los productos y servicios conforme a las necesidades específicas de cada cliente.',
        filosofy : [
            {
                title : 'Misión',
                icon : 'bi-flag',
                description : 'Destacarnos con excelencia en la aplicación de estrategias y tácticas de negociación, en la prestación de los Servicios de Cobranza, a los fines de afianzar competitivamente el crecimiento financiero de la empresa, con calidad de servicio y satisfacer las expectativas de nuestros clientes.',
            },
            {
                title : 'Visión',
                icon : 'bi-eye',
                description : 'Mantenernos como empresa pionera y líder de servicios de cobranza a escala nacional e internacional, ofreciendo soluciones integrales, basados en un recurso humano especializado, una plataforma tecnológica adecuada y las alianzas estratégicas necesarias para generar valor agregado a nuestros clientes.',
            }
        ],
        content : [
            {
                title : 'Estructura Organizativa',
                description : 'Quisque lobortis convallis mauris sit amet facilisis.',
                icon : 'bi-building',
                list : [
                    {
                        title : 'Estratégico',
                        description : 'Servir de enlace con nuestros aliados comerciales, garantizar el cumplimiento de los acuerdos de servicios.',
                        department : [
                            'Presidencia/Vicepresidencia'
                        ]
                    },
                    {
                        title : 'Táctico',
                        description : 'Garantizar la Operación desde el punto de vista de Recursos Humanos, Operaciones, Infraestructura y Comunicación.',
                        department : [
                            'Gerentes de Área',
                            'Coordinadores'
                        ]
                    },
                    {
                        title : 'Operativo',
                        description : 'Ejecutar las operaciones según la estrategia asignada bajo un esquema de métricas y scorecard.',
                        department : [
                            'Supervisores',
                            'Ejecutivos de Contacto',
                            'Back Office'
                        ]
                    },
                ]
            },
            {
                title : 'Estrategia de Calidad',
                description : 'Quisque lobortis convallis mauris sit amet facilisis.',
                icon : 'bi-star',
                content : [
                    {
                        title : 'Alianza Estratégica',
                        description : 'Quisque lobortis convallis mauris sit amet facilisis.',
                        icon : 'bi-bar-chart-steps',
                        features : [
                            'Adaptabilidad a las necesidades de negocio de nuestro cliente comercial',
                            'Capacidad de iniciar la operación bajo el esquema de Plan Piloto',
                            'Esquemas de trabajo  CO-Working',
                            'Ofrecemos diversidad de estructuras Operativas'
                        ]
                    },
                    {
                        title : 'Gestión',
                        description : 'Quisque lobortis convallis mauris sit amet facilisis.',
                        icon : 'bi-calendar-check',
                        features : [
                            'Capacidad de Operar de Lunes a Domingo',
                            'Flexibilidad de Manejo por bloques 1:10 (1 Supervisor x 10 Agentes telefónicos).',
                            'Sistema de grabación de llamadas.',
                            'Sistema de control de calidad.',
                            'Monitoreo permanente de la gestión.',
                            'Capacidad de Recepción de llamadas derivadas',
                            'Base de datos de localización.',
                            'Marcador telefónico automático, progresivo y predictivo.',
                            'IVR, SMS e E-MAIL (masivos).'
                        ]
                    },
                    {
                        title : 'Recursos',
                        description : 'Quisque lobortis convallis mauris sit amet facilisis.',
                        icon : 'bi-bank',
                        features : [
                            'Plataforma Tecnológica Ad-hoc.',
                            'Soporte continuo a nuestro aliado comercial a través de nuestros Ejecutivos de Contacto.',
                            'Estrategias de Captación del Recurso Humano y monitoreo continuo del capacity y vacantes.',
                            'Manejo de métricas y scorecards por cliente.',
                            'Proceso robusto de Auditorias y Controles.',
                            'Políticas de Seguridad de Información.'
                        ]
                    },
                    {
                        title : 'Preparados para la Contingencia',
                        description : 'Quisque lobortis convallis mauris sit amet facilisis.',
                        icon : 'bi-cloud-check',
                        features : [
                            'Ubicación: Nuestras oficinas están ubicadas en el Edificio Fedecamaras, dentro del sector donde esta ubicado el circuito eléctrico protegido de PDVSA.',
                            'Electricidad: Dos plantas eléctricas para proporcionar autonomía de 7 días.',
                            'Internet: Servicio dedicado con redundancia.',
                            'Transporte: Fácil acceso al transporte público o metro.',
                            'Seguridad: Custodiados por el personal de seguridad del Edificio. Control de Acceso.'
                        ]
                    },
                ]
            }
        ]
    },
    contact : {
        title : 'Contactanos ahora',
        subtitle : 'Tenemos lo que necesitas',
        description : 'Tecnología inteligente, excelencia en los procesos y análisis detallados para aumentar su base de clientes.',
    },
    work : {
        title : 'Nuestro trabajo y clientes',
        subtitle : 'Tenemos más de 40 años de experiencia en los servicios de Contact Center outsourcing',
        description : 'Tecnología inteligente, excelencia en los procesos y análisis detallados para aumentar su base de clientes.',
    }

}

export default IndexData;