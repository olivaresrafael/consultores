import { h } from 'preact';
import style from './style.css';
import Top from '../../components/top'
import model from '../../components/model'
import logos from '../../assets/img/logos.jpg'

const Nosotros = () => (
	<div class={style.home}>
        {/* <Top 
            title={model.about.title} 
            description={model.about.description} 
            button={false} 
            notHome={true}
        /> */}
        <MisionVision/>
        <Procesos/>
        <Alianza/>
        <Portafolio/>
	</div>
);

const Clients = () => (
    <section id="clients">
        <img src={logos} class="img-fluid"/>
    </section>
);

const MisionVision = () => (
    <section class="page-section py-5" id="mision">
        <div class="container px-4 px-lg-5 py-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-10 text-center">
                <div class="mb-2"><i class={model.about.icon + " fs-1 text-primary"}></i></div>
                    <h2 class={style.about_h + " text-center mt-0"}>Nuestra filosofía</h2>
                    <hr class="divider" />
                    <p class={style.about_p + ' mb-4 text-muted'}  >{model.about.subtitle}</p>
                    <div class="row gx-4 gx-lg-5 mt-5">
                        <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                            <div class="mb-2"><i class={model.about.filosofy[0].icon + " fs-1 text-primary"}></i></div>
                            <h3 class="h4 mb-2 text-primary">{model.about.filosofy[0].title}</h3>
                            <p class={`${style.servicios} text-muted mb-0`}>{model.about.filosofy[0].description}</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                            <div class="mb-2"><i class={model.about.filosofy[1].icon + " fs-1 text-primary"}></i></div>
                            <h3 class="h4 mb-2 text-primary">{model.about.filosofy[1].title}</h3>
                            <p class={`${style.servicios} text-muted mb-0`}>{model.about.filosofy[1].description}</p>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </section>
);

const Procesos = () => (
    <section class="page-section bg-primary py-5" id="procesos">
        <div class="container px-4 px-lg-5 py-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-12 text-center">
                    <div class="mb-2"><i class={model.about.content[1].icon + " fs-1 text-white mx-1"}></i><i class={model.about.content[1].icon + " fs-1 text-white mx-1"}></i><i class={model.about.content[1].icon + " fs-1 text-white mx-1"}></i></div>
                    <h2 class={style.about_h + " text-white mt-0"}>{model.about.content[1].title}</h2>
                    <hr class="divider divider-light" />
                    <p class={style.about_p + ' mb-4 text-white-75'}  >{model.about.content[1].description}</p>
                    <div class="row gx-4 gx-lg-5">
                        <Diagrama/>
                    </div>
                </div>
            </div>
        </div>
    </section>
);

const Alianza = () => (
    <section class="page-section bg-light pt-2" id="alianza">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-10 text-center">
                    <div class="row gx-4 gx-lg-5 mt-1">
                        {
                            model.about.content[1].content.map((process,key) => (
                                <div class="col-lg-12 col-md-12 col-xs-12 text-center pt-5">
                                    <div class="mb-2"><i class={process.icon + " fs-1 text-primary"}></i></div>
                                    <h3 class="h4 mb-2 ">{process.title}</h3>
                                    
                                    <ul class="list-group px-5 mt-4">
                                        {
                                            process.features.map((el) => (
                                                <li class="list-group-item list-group-item-light">{el}</li>
                                            ))
                                        }
                                    </ul>
                                </div>  
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    </section>
);

const Diagrama = () => (
    <>
        <div class="d-flex col-lg-3">
            <p class="h5 align-self-center float-start my-0 text-white">Consultores SC </p>
            <i class="bi-arrow-right-circle text-white align-self-center float-start ms-2 fs-3"/>
        </div>
        <div class="col-lg-6 rounded">
            <ul class="list-group list-group-horizontal justify-content-center">
                <li class={"d-flex list-group-item-primary justify-content-center " + style.quart}>
                    <div class="d-flex align-self-center ">Alianza estrategica</div>
                </li>
                <li class={"d-flex list-group-item-secondary justify-content-center " + style.quart}>
                    <div class="d-flex align-self-center ">Preparado para la contingencia</div>
                </li>
            </ul>
            <ul class="list-group list-group-horizontal">
                <li class={"d-flex list-group-item-info justify-content-center " + style.quart}>
                    <div class="d-flex align-self-center ">Recursos</div>
                </li>
                <li class={"d-flex list-group-item-warning justify-content-center " + style.quart}>
                    <div class="d-flex align-self-center ">Gestión</div>
                </li>
            </ul>
        </div>
        <div class="d-flex col-lg-3">
            <i class="bi-arrow-left-circle text-white align-self-center float-start me-2 fs-3"/>
            <p class="h5 align-self-center float-start my-0 text-white">Aliados Comerciales</p>
        </div>
    </>
)



const Estructura = () => (
    <section class="page-section" id="agency">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-10 text-center">
                    <div class="mb-2"><i class={model.services.list[0].icon + " fs-1 text-primary"}></i></div>
                    <h2 class={style.about_h + " text-center mt-0"}>{model.services.list[0].title}</h2>
                    <hr class="divider" />
                    <p class={style.about_p + ' mb-4 text-muted'}  >{model.services.list[0].description}</p>
                    <div class="row gx-4 gx-lg-5">
                    {
                        model.services.list[0].sub.map((service,key) => {
                            return (
                                <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                                <div class="mt-5">
                                    <div class="mb-2"><i class={service.icon + " fs-1 text-primary"}></i></div>
                                    <h3 class="h4 mb-2 text-primary">{service.title}</h3>
                                    <p class={`${style.servicios} text-muted mb-0`}>{service.description}</p>
                                </div>
                            </div>
                            )
                        })
                    }
                    </div>
                </div>
            </div>
        </div>
    </section>
);



const Portafolio = () => (
        <div id="portfolio">
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/1.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/2.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/2.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/3.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/3.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/4.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/4.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/5.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/5.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/6.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/6.jpg" alt="..." />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
);


const Acordion = () => (
    <div class="accordion" id="accordionPanelsStayOpenExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingOne">
    <button class="col-lg-12 col-md-12 col-xs-12 text-center pt-5" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseOne" aria-expanded="true" aria-controls="panelsStayOpen-collapseOne">
                <div class="mb-2"><i class={"bi-recycle fs-1 text-primary"}></i></div>
                <h3 class="h4 mb-2 text-primary">asdfasdfasd adfa df</h3>
                <p class={`${style.servicios} text-muted mb-0 text-center`}>asdfasdf asdf asdfas dfadfa afd fafa dfdfad</p>
    </button>  

    </h2>

    <div id="panelsStayOpen-collapseOne" class="accordion-collapse collapse show" aria-labelledby="panelsStayOpen-headingOne">
      <div class="accordion-body">
        <strong>This is the first item's accordion body.</strong> It is shown by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseTwo" aria-expanded="false" aria-controls="panelsStayOpen-collapseTwo">
        Accordion Item #2
      </button>
    </h2>
    <div id="panelsStayOpen-collapseTwo" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingTwo">
      <div class="accordion-body">
        <strong>This is the second item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="panelsStayOpen-headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#panelsStayOpen-collapseThree" aria-expanded="false" aria-controls="panelsStayOpen-collapseThree">
        Accordion Item #3
      </button>
    </h2>
    <div id="panelsStayOpen-collapseThree" class="accordion-collapse collapse" aria-labelledby="panelsStayOpen-headingThree">
      <div class="accordion-body">
        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>
</div>
)


export default Nosotros;
