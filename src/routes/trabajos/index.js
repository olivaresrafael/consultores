import { h } from 'preact';
import style from './style.css';
import Top from '../../components/top'
import model from '../../components/model'
import logos from '../../assets/img/logos.jpg'

const Trabajos = () => (
	<div class={style.home}>
        {/* <Top 
            title={model.work.title} 
            description={model.work.description} 
            button={false} 
            notHome={true}
        /> */}
        <About/>
        <Clients/>
        <Services/>      
        <Portafolio/>
	</div>
);

const Clients = () => (
    <section id="clients">
        <img src={logos} class="img-fluid"/>
    </section>
);

const About = () => (
    <section class="page-section bg-primary" id="about">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-8 text-center">
                    <h2 class={style.about_h + " text-white mt-0"}>{model.about.title}</h2>
                    <hr class="divider divider-light" />
                    <p class={style.about_p + ' text-white-75 mb-4'}  >{model.about.description}</p>
                    <a class="btn btn-light btn-xl" href="#services">Conoce sobre nosotros</a>
                </div>
            </div>
        </div>
    </section>
);

const Services = () => (
        <section class="page-section" id="services">
            <div class="container px-4 px-lg-5">
                <h2 class={`${style.servicio_title} text-center mt-0`}>{model.services.title}</h2>
                <hr class="divider" />
                <div class="row gx-4 gx-lg-5">
                {
                    model.services.list.map((service,key) => {
                        return (
                            <div class="col-lg-4 col-md-6 col-xs-12 text-center">
                            <div class="mt-5">
                                <div class="mb-2"><i class={service.icon + " fs-1 text-primary"}></i></div>
                                <h3 class="h4 mb-2">{service.title}</h3>
                                <p class={`${style.servicios} text-muted mb-0`}>{service.description}</p>
                            </div>
                        </div>
                        )
                    })
                }
                </div>
                <hr class="divider" />
                <div class="col-md-8  offset-md-2 col-xs-12 align-items-center justify-content-center text-center">
                    <p class={style.servicio_description + " mb-5"}>{model.services.description}</p>
                    <a class="btn btn-primary btn-xl" href="#about">Explora nuestros servicos</a>
                </div>
            </div>
        </section>
);

const Portafolio = () => (
        <div id="portfolio">
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/1.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/2.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/2.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/3.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/3.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/4.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/4.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/5.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/5.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/6.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/6.jpg" alt="..." />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
);

const Block = () => (
    <section class="page-section bg-dark text-white">
        <div class="container px-4 px-lg-5 text-center">
            <h2 class="mb-4">Descubre nuestras instalaciones</h2>
            <a class="btn btn-light btn-xl" href="https://startbootstrap.com/theme/creative/">Sobre nosotros</a>
        </div>
    </section>
);

const Contact = () => (
    <section class="page-section" id="contact">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-8 col-xl-6 text-center">
                    <h2 class="mt-0">Contactá con nosotros</h2>
                    <hr class="divider" />
                    <p class="text-muted mb-5">¿Listo para empezar su próximo proyecto con nosotros? Envíenos un mensaje y nos pondremos en contacto con usted lo antes posible.</p>
                </div>
            </div>
            <div class="row gx-4 gx-lg-5 justify-content-center mb-5">
                <div class="col-lg-6">



                    <form id="contactForm" data-sb-form-api-token="API_TOKEN">
            
                        <div class="form-floating mb-3">
                            <input class="form-control" id="name" type="text" placeholder="Enter your name..." data-sb-validations="required" />
                            <label for="name">Nombre y apellido</label>
                            <div class="invalid-feedback" data-sb-feedback="name:required">A name is required.</div>
                        </div>
        
                        <div class="form-floating mb-3">
                            <input class="form-control" id="email" type="email" placeholder="name@example.com" data-sb-validations="required,email" />
                            <label for="email">Email</label>
                            <div class="invalid-feedback" data-sb-feedback="email:required">An email is required.</div>
                            <div class="invalid-feedback" data-sb-feedback="email:email">Email is not valid.</div>
                        </div>

                        <div class="form-floating mb-3">
                            <input class="form-control" id="phone" type="tel" placeholder="(123) 456-7890" data-sb-validations="required" />
                            <label for="phone">Teléfono</label>
                            <div class="invalid-feedback" data-sb-feedback="phone:required">A phone number is required.</div>
                        </div>

                        <div class="form-floating mb-3">
                            <textarea class="form-control" id="message" type="text" placeholder="Enter your message here..." style="height: 10rem" data-sb-validations="required"></textarea>
                            <label for="message">Mensaje</label>
                            <div class="invalid-feedback" data-sb-feedback="message:required">A message is required.</div>
                        </div>

                        <div class="d-none" id="submitSuccessMessage">
                            <div class="text-center mb-3">
                                <div class="fw-bolder">Form submission successful!</div>
                                To activate this form, sign up at
                                <br />
                                <a href="https://startbootstrap.com/solution/contact-forms">https://startbootstrap.com/solution/contact-forms</a>
                            </div>
                        </div>
                        <div class="d-none" id="submitErrorMessage"><div class="text-center text-danger mb-3">Error sending message!</div></div>
                    
                        <div class="d-grid"><button class="btn btn-primary btn-xl disabled" id="submitButton" type="submit">Enviar</button></div>
                    </form>
                </div>
            </div>
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-4 text-center mb-5 mb-lg-0">
                    <i class="bi-phone fs-2 mb-3 text-muted"></i>
                    <div>+58 (212) 123-4567</div>
                </div>
            </div>
        </div>
    </section>
);

export default Trabajos;
