import { h } from 'preact';
import style from './style.css';
import Top from '../../components/top'
import model from '../../components/model'
import logos from '../../assets/img/logos.jpg'

const Servicios = () => (
	<div class={style.home}>
        {/* <Top 
            title={model.services.title} 
            description={model.services.description} 
            button={false} 
            notHome={true}
        /> */}
        <Agency/>
        <ContactCenter/>
        <BPO/>      
        <Portafolio/>
	</div>
);

const Clients = () => (
    <section id="clients">
        <img src={logos} class="img-fluid"/>
    </section>
);

const Agency = () => (
    <section class="page-section" id="agency">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-10 text-center">
                    <div class="mb-2"><i class={model.services.list[0].icon + " fs-1 text-primary"}></i></div>
                    <h2 class={style.about_h + " text-center mt-0"}>{model.services.list[0].title}</h2>
                    <hr class="divider" />
                    <p class={style.about_p + ' mb-4 text-muted'}  >{model.services.list[0].description}</p>
                    <div class="row gx-4 gx-lg-5">
                    {
                        model.services.list[0].sub.map((service,key) => {
                            return (
                                <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                                <div class="mt-5">
                                    <div class="mb-2"><i class={service.icon + " fs-1 text-primary"}></i></div>
                                    <h3 class="h4 mb-2 text-primary">{service.title}</h3>
                                    <p class={`${style.servicios} text-muted mb-0`}>{service.description}</p>
                                </div>
                            </div>
                            )
                        })
                    }
                    </div>
                </div>
            </div>
        </div>
    </section>
);

const ContactCenter = () => (
    <section class="page-section bg-primary" id="ccenter">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-10 text-center">
                <div class="mb-2"><i class={model.services.list[1].icon + " fs-1 text-white"}></i></div>
                    <h2 class={style.about_h + " text-white mt-0"}>{model.services.list[1].title}</h2>
                    <hr class="divider divider-light" />
                    <p class={style.about_p + ' mb-4 text-white-75'}  >{model.services.list[1].description}</p>
                    <div class="row gx-4 gx-lg-5">
                    {
                        model.services.list[1].sub.map((service,key) => {
                            return (
                                <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                                <div class="mt-5">
                                    <div class="mb-2"><i class={service.icon + " fs-1 text-white"}></i></div>
                                    <h3 class="h4 mb-2 text-white">{service.title}</h3>
                                    <p class={`${style.servicios} text-white-75 mb-0`}>{service.description}</p>
                                    <ul class="list-group px-5 mt-4">
                                    {service.list.map((el,k) => [
                                            <li class="list-group-item list-group-item-primary">{el}</li>
                                        ])}
                                    </ul>
                                </div>
                            </div>
                            )
                        })
                    }
                    </div>
                </div>
            </div>
        </div>
    </section>
);

const BPO = () => (
    <section class="page-section" id="bpo">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-10 text-center">
                <div class="mb-2"><i class={model.services.list[2].icon + " fs-1 text-primary"}></i></div>
                    <h2 class={style.about_h + " text-center mt-0"}>{model.services.list[2].title}</h2>
                    <hr class="divider" />
                    <p class={style.about_p + ' mb-4 text-muted'}  >{model.services.list[2].description}</p>
                    <div class="row gx-4 gx-lg-5">
                    {
                        model.services.list[2].list.map((service,key) => {
                            return (
                                <div class={`col-lg-4 col-md-4 col-xs-12 text-center ${key==3?'offset-md-2':''} ${key>2?'mt-md-4':''}`}>
                                    <div class="mb-2"><i class={service[1] + " fs-1 text-primary"}></i></div>
                                    <h3 class="h4 mb-2 text-muted">{service[0]}</h3>
                                </div>
                            )
                        })
                    }
                    </div>
                </div>
            </div>
        </div>
    </section>
);

const Portafolio = () => (
        <div id="portfolio">
            <div class="container-fluid p-0">
                <div class="row g-0">
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/1.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/1.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/2.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/2.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/3.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/3.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/4.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/4.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/5.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/5.jpg" alt="..." />
                            <div class="portfolio-box-caption">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a class="portfolio-box" href="assets/img/portfolio/fullsize/6.jpg" title="Project Name">
                            <img class="img-fluid" src="assets/img/portfolio/thumbnails/6.jpg" alt="..." />
                            <div class="portfolio-box-caption p-3">
                                <div class="project-category text-white-50">Category</div>
                                <div class="project-name">Project Name</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
);




export default Servicios;
