import { h } from 'preact';
import style from './style.css';
import Top from '../../components/top'
import model from '../../components/model'
import { useState } from 'preact/hooks';

const Contacto = () => {
    return (
        <div class={style.home}>
            {/* <Top 
                title={model.contact.title} 
                description={model.contact.description} 
                button={false} 
                notHome={true}
            /> */}
            <Maps/>
            <Contact/>
        </div>
    )
};



const Block = () => (
    <section class="page-section bg-dark text-white">
        <div class="container px-4 px-lg-5 text-center">
            <h2 class="mb-4">Descubre nuestras instalaciones</h2>
            <a class="btn btn-light btn-xl" href="https://startbootstrap.com/theme/creative/">Sobre nosotros</a>
        </div>
    </section>
);
const Maps = () =>  {

    const [loading, setLoading] = useState(true);
    const [size, setSize] = useState(['0', '0']);

    const hideSpinner = () => {
        setLoading(false);
        setSize(['100%','300px'])
    }

    return (
        <div class={style.maprouter}>
            <div class={style.gmap_canvas + ' d-flex justify-content-center'}>
                { loading &&
                    <div class="d-flex align-self-center">
                        <div class="spinner-border" style="width: 3rem; height: 3rem;" role="status">
                            <span class="visually-hidden">Loading...</span>
                        </div>
                    </div>
                }

                    <iframe 
                        width={size[0]} 
                        height={size[1]}  
                        id="gmap_canvas" 
                        onLoad={hideSpinner}
                        src="https://maps.google.com/maps?q=Fedecamaras,%20Avenida%20El%20C%C3%A1rmen,%20Caracas,%20Distrito%20Capital&t=&z=19&ie=UTF8&iwloc=&output=embed"
                        frameborder="0" 
                        scrolling="no" 
                        marginheight="0" 
                    marginwidth="0"/>

                <br/>
                
            </div>
        </div>
    )
}

const Contact = () => (
    <section class="page-section" id="contact">
        <div class="container px-4 px-lg-5">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-8 col-xl-6 text-center">
                    <h2 class="mt-0">Contactá con nosotros</h2>
                    <hr class="divider" />
                    <p class="text-muted mb-5">¿Listo para empezar su próximo proyecto con nosotros? Envíenos un mensaje y nos pondremos en contacto con usted lo antes posible.</p>
                </div>
            </div>
            <div class="row gx-4 gx-lg-5 justify-content-center mb-5">
                <div class="col-lg-6">



                    <form id="contactForm" data-sb-form-api-token="API_TOKEN">
            
                        <div class="form-floating mb-3">
                            <input class="form-control" id="name" type="text" placeholder="Enter your name..." data-sb-validations="required" />
                            <label for="name">Nombre y apellido</label>
                            <div class="invalid-feedback" data-sb-feedback="name:required">A name is required.</div>
                        </div>
        
                        <div class="form-floating mb-3">
                            <input class="form-control" id="email" type="email" placeholder="name@example.com" data-sb-validations="required,email" />
                            <label for="email">Email</label>
                            <div class="invalid-feedback" data-sb-feedback="email:required">An email is required.</div>
                            <div class="invalid-feedback" data-sb-feedback="email:email">Email is not valid.</div>
                        </div>

                        <div class="form-floating mb-3">
                            <input class="form-control" id="phone" type="tel" placeholder="(123) 456-7890" data-sb-validations="required" />
                            <label for="phone">Teléfono</label>
                            <div class="invalid-feedback" data-sb-feedback="phone:required">A phone number is required.</div>
                        </div>

                        <div class="form-floating mb-3">
                            <textarea class="form-control" id="message" type="text" placeholder="Enter your message here..." style="height: 10rem" data-sb-validations="required"></textarea>
                            <label for="message">Mensaje</label>
                            <div class="invalid-feedback" data-sb-feedback="message:required">A message is required.</div>
                        </div>

                        <div class="d-none" id="submitSuccessMessage">
                            <div class="text-center mb-3">
                                <div class="fw-bolder">Form submission successful!</div>
                                To activate this form, sign up at
                                <br />
                                <a href="https://startbootstrap.com/solution/contact-forms">https://startbootstrap.com/solution/contact-forms</a>
                            </div>
                        </div>
                        <div class="d-none" id="submitErrorMessage"><div class="text-center text-danger mb-3">Error sending message!</div></div>
                    
                        <div class="d-grid"><button class="btn btn-primary btn-xl disabled" id="submitButton" type="submit">Enviar</button></div>
                    </form>
                </div>
            </div>
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-4 text-center mb-5 mb-lg-0">
                    <i class="bi-phone fs-2 mb-3 text-muted"></i>
                    <div>+58 (212) 123-4567</div>
                </div>
            </div>
        </div>
    </section>
);

export default Contacto;
